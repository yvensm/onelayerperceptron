import perceptron
import numpy as np
import matplotlib.pyplot as plt
import random
import FunctionsMethods as fm

class PUC(object):

    def __init__(self, dataset = [], eta=0.1, epochs=50, percTraining = 0.8,opt=0):
        self.dataset= dataset
        self.eta = dataset
        self.eta = eta
        self.epochs = epochs
        self.percTraining = percTraining
        self.c = len(dataset[0][-1])
        self.neurons = []
        self.opt=opt
        self.acc = 0
        self.weights = []
        self.erros_ = []
        self.trainData = self.dataset[0:int(len(self.dataset) * percTraining) - 1, range(len(self.dataset[0]) - 1)]
        self.trainDesire = self.dataset[0:int(len(self.dataset) * percTraining) - 1, -1]
        self.testData = self.dataset[int(len(self.dataset) * percTraining) - 1:len(self.dataset),
                        range(len(self.dataset[0]) - 1)]
        self.testDesire = self.dataset[int(len(self.dataset) * percTraining) - 1:len(self.dataset),
                          -1]
        self.y_test = []

        # for n in range(c):
        #     d=[]
        #     data=self.dataset[:,range(len(self.dataset[0])-1)]
        #     for i in range(len(self.dataset)):
        #         d.append(self.dataset[i][len(self.dataset[0])-1][n])
        #     d = np.reshape(d, (len(d), 1))
        #     data = np.hstack((data,d))
        #     self.neurons.append(perceptron.Perceptron(data,self.eta,self.epochs,self.percTraining,self.opt))


    def train(self):
        self.erros_ = np.zeros((self.epochs,), dtype=int)
        # self.weights = [-1, random.random(), random.random()]
        self.weights = np.random.rand(self.c,len(self.trainData[0]))#np.zeros((len(self.trainData[0]),self.c), dtype=float)
        X = self.trainData
        d = self.trainDesire
        for e in range(self.epochs):
            errors = 0
            self.shuffleData(X, d)
            for xi, di in zip(X, d):
                y = self.predict(xi)
                yd = fm.derivative(self.getY(self.getU(xi)),self.opt)
                update = self.eta * (di - y) * yd #fm.derivative(y, self.opt)
                for i in range(len(update)):
                    self.weights[i] = self.weights[i] + np.array(update[i] * xi)
                for i in update:
                    if i != 0.0:
                        errors += 1
            if errors == 0:
                break
            self.erros_[e] = errors
        return self


    def test(self):
        X = self.testData
        d = self.testDesire
        erros = 0
        acertos = 0
        y_test = []
        for xi, di in zip(X, d):
            y = self.predict(xi)
            y_test.append(y)
            if np.array_equal(y.astype(int),np.array(di)):
                acertos += 1
            else:
                erros += 1
        self.y_test = y_test
        self.acc = (acertos / (erros + acertos))

    def predict(self,x):
        u=np.zeros((self.c,),dtype=float)
        for i in range(len(u)):
            u[i] = fm.functionU(np.dot(x,self.weights[i]), self.opt)
        u= self.maxmize(u)
        return u

    def getU(self,x):
        u = np.zeros((self.c,), dtype=float)
        for i in range(len(u)):
            u[i] = np.dot(x, self.weights[i])
        return u

    def getY(self,u):
        for i in range(len(u)):
            u[i] = fm.functionU(u[i], self.opt)
        return u

    def maxmize(self,pred):
        max_ = max(pred)
        for i in range(self.c):
            if pred[i] == max_:
                pred[i] = 1
            else:
                if self.opt == 2:
                    pred[i] = -1
                else:
                    pred[i] = 0

        return np.array(pred)
    def plotData2d(self):
        resolution=50
        aux=0
        if self.opt == 2:
            aux=-1
        for i in range(resolution):
            for j in range(resolution):
                if np.array_equal(self.predict(np.asarray([-1,i/float(resolution),j/float(resolution)])), np.array([1,aux,aux])):
                    plt.scatter(i / float(resolution), j / float(resolution), s=5, c='r', alpha=0.3)
                elif np.array_equal(self.predict(np.asarray([-1,i/float(resolution),j/float(resolution)])), np.array([aux,1,aux])):
                    plt.scatter(i / float(resolution), j /float(resolution), s=5, c='g', alpha=0.3)
                elif np.array_equal(self.predict(np.asarray([-1,i/float(resolution),j/float(resolution)])), np.array([aux,aux,1])):
                    plt.scatter(i / float(resolution), j /float(resolution), s=5, c='b',alpha=0.3)
                else:
                    plt.scatter(i / float(resolution), j / float(resolution), s=5, c='w')




        for i in range(len(self.trainDesire)):
            if np.array_equal(self.trainDesire[i],[aux,aux,1]):
                plt.scatter(self.trainData[i][1], self.trainData[i][-1], s=10, c='r',marker='^')
            elif np.array_equal(self.trainDesire[i],[aux,1,aux]):
                plt.scatter(self.trainData[i][1], self.trainData[i][-1], s=10, c='g',marker='*')
            elif np.array_equal(self.trainDesire[i],[1,aux,aux]):
                plt.scatter(self.trainData[i][1], self.trainData[i][-1], s=10, c='b',marker='s')

        for i in range(len(self.testDesire)):
            if np.array_equal(self.testDesire[i],[aux,aux,1]):
                plt.scatter(self.testData[i][1], self.testData[i][-1], s=10, c='k',marker='^')
            elif np.array_equal(self.testDesire[i],[aux,1,aux]):
                plt.scatter(self.testData[i][1], self.testData[i][-1], s=10, c='k',marker='*')
            elif np.array_equal(self.testDesire[i],[1,aux,aux]):
                plt.scatter(self.testData[i][1], self.testData[i][-1], s=10, c='k',marker='s')
        plt.show()

    def shuffleData(self,X,y):
        dataset=[]
        for i,j in zip(X, y):
            dataset.append([i, j])
        np.random.shuffle(dataset)
        dataset = np.asarray(dataset)
        X=dataset[:,range(len(dataset[0])-1)]
        y=dataset[:,-1]


