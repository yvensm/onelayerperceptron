import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import random as rd
import PUC


def readData(path,datasetname,op):
    datasetlocation = path + datasetname
    data = np.array(pd.read_csv(datasetlocation, delimiter=',', header=None))
    return classSelection(data,op)

def classSelection(datasetoriginal, opt):
    #labels = np.array([])
    dataset = datasetoriginal
    if opt == 0 or opt == 1:
        #labels = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
        aux = 0
    else:
        #labels = [[1, -1, -1], [-1, 1, -1], [-1, -1, 1]]
        aux = -1

    iris = np.unique(dataset[:, -1])
    for i in dataset:
        if i[-1] == iris[0]:
            i[-1] = [1, aux, aux]
        elif i[-1] == iris[1]:
            i[-1] = [aux, 1, aux]
        else:
            i[-1] = [aux, aux, 1]

    return dataset

def generateArtificial(op):
    dataset = []
    if op == 0 or op ==1:
        aux = 0
    else:
        aux= -1

    for i in range(150):
        if i < 50:
            x=0.1 + np.random.uniform(-0.1,0.1)
            y=0.5 + np.random.uniform(-0.1,0.1)
            label=[1,aux,aux]
        elif i<100:
            x = 0.6+np.random.uniform(-0.1, 0.1)
            y = 0.5 + np.random.uniform(-0.1, 0.1)
            label = [aux, 1, aux]
        else:
            x = 0.35 + np.random.uniform(-0.1, 0.1)
            y = 0.2 + np.random.uniform(-0.1, 0.1)
            label = [aux, aux, 1]
        dataset.append([x, y, label])

    return np.array(dataset)


def insertbias(dataset):
    new = []
    for i in range(len(dataset)):
        new.append(np.insert(dataset[i], 0, -1))
    return np.asarray(new)

def normalize(dataset):
    for i in range(dataset.shape[1]-1):
        max_ = max(dataset[:, i])
        min_ = min(dataset[:, i])
        for j in range(dataset.shape[0]):
            dataset[j, i] = (dataset[j, i] - min_) / (max_ - min_)
    return dataset


def plotData2d(dataset, w):
    # y =[]
    # for i in dataset:
    #     y.append(np.dot(w, i[0:len(i) - 1]))

    plt.scatter(dataset[:, 1], dataset[:, -2], s=5, c='r')
    # plt.plot(dataset[:, 1], y, color='c')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title('Adaline')
    plt.show()

print("Selecione a função \n1-Funcao degrau\n2-Funcao Logistica\n3-Funcao Tangente Hiperbolica")
fsel = int(input())-1
print("1 - Artificial")
print("2 - Íris 1")
print("3 - Íris 2")
datasel=int(input())
if datasel== 1:
    dataset=generateArtificial(fsel)
    np.random.shuffle(dataset)
    dataset=insertbias(dataset)
elif datasel == 2:
    dataset=readData("","iris.data",fsel)
    normalize(dataset)
    dataset=insertbias(dataset)
    dataset= dataset[:,[0,1,3,-1]]
    np.random.shuffle(dataset)
else:
    dataset = readData("", "iris.data", fsel)
    normalize(dataset)
    dataset = insertbias(dataset)
    np.random.shuffle(dataset)
# print
acc_ = []
redes = []
for i in range(20):
    slp = PUC.PUC(dataset,0.05,200,0.8,fsel)
    slp.train()
    slp.test()
    redes.append(slp)
    acc_.append(slp.acc)
    print("Acerto : %.8lf" % slp.acc)
print("Acuracia : %.8lf" %np.mean(acc_))
print("Desvio padrao : %.8lf" %np.array(acc_).std())

m=0
for i in range(20):
    if redes[m].acc<redes[i].acc:
        m=i
slp=redes[i]

if len(dataset[0])-1<=3:
    slp.plotData2d()