import numpy as np
import random
import FunctionsMethods as fm

class Perceptron(object):

    def __init__(self, dataset=[], eta=0.1, epochs=50,percTraining= 0.8,opt=0):
        self.eta = eta
        self.dataset = dataset
        self.epochs = epochs
        self.weights = []
        self.erros_ = []
        self.trainData = self.dataset[0:int(len(self.dataset) * percTraining) - 1, range(len(self.dataset[0]) - 1)]
        self.trainDesire = self.dataset[0:int(len(self.dataset) * percTraining) - 1, -1]
        self.testData = self.dataset[int(len(self.dataset) * percTraining) - 1:len(self.dataset),
                        range(len(self.dataset[0]) - 1)]
        self.testDesire = self.dataset[int(len(self.dataset) * percTraining) - 1:len(self.dataset),
                          -1]
        self.y_test = []
        self.acc = 0
        self.opt=opt
        self.c = len(dataset[0][-1])

    def train(self):
        self.erros_ = np.zeros((self.epochs,), dtype=int)
        self.weights = np.zeros((len(self.trainData[0]),len(self.trainDesire[0])), dtype=float)
        for i in range(len(self.weights)):
            for j in self.weights[i]:
                j = random.random()
        X=self.trainData
        d=self.trainDesire
        for e in range(self.epochs):
            errors = 0
            self.shuffleData(X,d)
            for xi, di in zip(X,d):
                y = self.predict(xi)
                update = self.eta * (di - y) * fm.derivative(y,self.opt)
                for i in range(len(X[0])):
                    self.weights[i] += update * xi[i]
                errors += int(update != 0.0)
            if errors == 0:
                break
            self.erros_[e]=errors
        return self

    def predict(self,X):
        u=np.zeros()
        u = np.dot(X, self.weights)
        return fm.functionU(u,self.opt)
        # return np.where(u >= 0.0, 1, 0)

    def test(self):
        X = self.testData
        y= self.testDesire
        erros = 0
        acertos = 0
        y_test=[]
        for xi, d in zip(X, y):
            y = self.predict(xi)
            y_test.append(y)
            if y == d:
                acertos += 1
            else:
                erros += 1
        self.y_test=y_test
        self.acc=(acertos / (erros + acertos))


    def shuffleData(self,X,y):
        dataset=[]
        for i,j in zip(X, y):
            dataset.append([i, j])
        np.random.shuffle(dataset)
        dataset = np.asarray(dataset)
        X=dataset[:,range(len(dataset[0])-1)]
        y=dataset[:,-1]

