import math
import numpy as np


def functionU(u, opt):
    if opt == 0:
        return u
    elif opt == 1:
        return logsig(u)
    else:
        return np.tanh(u)


def step(u):
    return np.where(u >= 0.0, 1, 0)


def logsig(u):
    return 1 / (1 + math.exp(-u))


def derivative(y, opt):
    if opt == 0:
        return 1
    if opt == 1:
        return y*(1-y)
    else:
        return (1/2)*(1 - y*y)